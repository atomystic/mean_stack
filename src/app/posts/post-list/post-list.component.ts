import { Component, OnInit, Input, Output, OnDestroy } from '@angular/core';
import { Post } from '../post-modele';
import { PostService } from '../post.service';
import { Subscription } from "rxjs";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
  // posts=[
  //   {title:"First Post", content:"This is the first post's content"},
  //   {title:"Second Post", content:"This is the second post's content"},
  //   {title:"Third Post", content:"This is the third post's content"},
  // ]

  posts: Post[] = [];
  private postSub: Subscription;
  constructor(private PostsService: PostService) { }

  ngOnInit(): void {
    this.posts = this.PostsService.getPosts();
    this.postSub = this.PostsService.getPostUpdateListener().subscribe((posts: Post[]) => {
      this.posts = posts;
    });
  }

  ngOnDestroy() {
    this.postSub.unsubscribe();
  }



}
