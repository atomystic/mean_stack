import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Post } from '../post-modele';
import { NgForm } from '@angular/forms';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css'],
})
export class PostCreateComponent implements OnInit {
  enteredTitle = '';
  enteredContent = '';


  constructor(private PostsService: PostService) { }

  ngOnInit(): void { }

  OnAddPost(form: NgForm) {
    if (form.invalid) {
      return;
    }

    this.PostsService.addPosts(form.value.title, form.value.content);
    form.resetForm();

  }
}
