import { Injectable } from '@angular/core';
import { Post } from './post-modele';
import { PostListComponent } from './post-list/post-list.component';
import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();
  constructor() { }

  // méthode
  getPosts() {
    return this.posts;
  }

getPostUpdateListener(){

  return this.postsUpdated.asObservable();
}
  addPosts(title: string, content: string) {
    const post: Post = {
      title: title,
      content: content
    }
    this.posts.push(post);
    this.postsUpdated.next([...this.posts])
  }

}
